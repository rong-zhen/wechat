package com.example.mywechat;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link contactFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class contactFragment extends Fragment {
    private View view;//定义view用来设置fragment的layout
    public RecyclerView recyclerView;//定义RecyclerView
    //定义以mDataList实体类为对象的数据集合
    private List<String> mList = new ArrayList<>();
    private List<contactData> mDataList = new ArrayList<contactData>();
    //自定义recyclerveiw的适配器
    private contactAdapter adapter;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public contactFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment weixinFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static contactFragment newInstance(String param1, String param2) {
        contactFragment fragment = new contactFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.tab03, container, false);
        initView();
        initList();
        initData();
        return view;
    }

    private void initList() {
        mList.add("漫威英雄电话-美国队长");
        mList.add("漫威英雄电话-绿巨人");
        mList.add("漫威英雄电话-钢铁侠");
        mList.add("漫威英雄电话-雷神");
        mList.add("漫威英雄电话-黑寡妇");
        mList.add("漫威英雄电话-鹰眼");
        mList.add("漫威英雄电话-蚁人");
        mList.add("漫威英雄电话-蜘蛛侠");
        mList.add("漫威英雄电话-死侍");
        mList.add("DC英雄电话-蝙蝠侠");
        mList.add("DC英雄电话-超人");
        mList.add("DC英雄电话-神奇女侠");
        mList.add("DC英雄电话-闪电侠");
        mList.add("DC英雄电话-钢骨");
        mList.add("DC英雄电话-海王");
        mList.add("中国超级英雄电话-孙悟空");
        mList.add("中国超级英雄电话-哪吒");
        mList.add("中国超级英雄电话-杨戬");
        mList.add("中国超级英雄电话-姜子牙");
        mList.add("中国超级英雄电话-雷震子");
        mList.add("我的电话-123456789");
    }

    private void initData() {
        for (int i = 0; i < mList.size(); i++) {
            contactData bean = new contactData();

            String s = mList.get(i);
            // group
            String group = s.substring(0, s.indexOf("-"));
            // name
            String team = s.substring(s.indexOf("-") + 1, s.length());

            bean.setGroup(group);
            bean.setName(team);

            mDataList.add(bean);
        }


    }

    private void initView() {
        RecyclerView recyclerView = view.findViewById(R.id.tab03_1);
        adapter = new contactAdapter(getActivity());


        final TextView tvGroup = view.findViewById(R.id.tab03_header_view);

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        adapter.setContactDataList(mDataList);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                View stickyInfoView = recyclerView.findChildViewUnder(
                        tvGroup.getMeasuredWidth() / 2, 5);

                if (stickyInfoView != null && stickyInfoView.getContentDescription() != null) {
                    tvGroup.setText(String.valueOf(stickyInfoView.getContentDescription()));
                }

                View transInfoView = recyclerView.findChildViewUnder(
                        tvGroup.getMeasuredWidth() / 2, tvGroup.getMeasuredHeight() + 1);

                if (transInfoView != null && transInfoView.getTag() != null) {

                    int transViewStatus = (int) transInfoView.getTag();
                    int dealtY = transInfoView.getTop() - tvGroup.getMeasuredHeight();

                    if (transViewStatus == contactAdapter.HAS_STICKY_VIEW) {
                        if (transInfoView.getTop() > 0) {
                            tvGroup.setTranslationY(dealtY);
                        } else {
                            tvGroup.setTranslationY(0);
                        }
                    } else if (transViewStatus == contactAdapter.NONE_STICKY_VIEW) {
                        tvGroup.setTranslationY(0);
                    }
                }
            }
        });
    }
}
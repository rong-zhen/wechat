
package com.example.mywechat;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class MainActivity extends Activity implements View.OnClickListener {

    private  LinearLayout xmlweixin;
    private  LinearLayout xmlfrd;
    private  LinearLayout xmlcontact;
    private  LinearLayout xmlsettings;

    private ImageButton xmlimageweixin;
    private ImageButton xmlimagefrd;
    private ImageButton xmlimagecontact;
    private ImageButton xmlimagesettings;

    private Fragment mTab01=new weixinFragment();
    private Fragment mTab02=new frdFragment();
    private Fragment mTab03=new contactFragment();
    private Fragment mTab04=new settingsFragment();

    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        initView();
        initEvent();
        initFragment();
        setSelect(0);
    }

    private void initFragment(){
        fm = getFragmentManager();
        FragmentTransaction transaction= fm.beginTransaction();
        transaction.add(R.id.id_content,mTab01);
        transaction.add(R.id.id_content,mTab02);
        transaction.add(R.id.id_content,mTab03);
        transaction.add(R.id.id_content,mTab04);
        transaction.commit();
    }

    private void initView(){
        xmlweixin=(LinearLayout) findViewById(R.id.id_tab_weixin);
        xmlfrd=(LinearLayout) findViewById(R.id.id_tab_frd);
        xmlcontact=(LinearLayout) findViewById(R.id.id_tab_contact);
        xmlsettings=(LinearLayout) findViewById(R.id.id_tab_settings);

        xmlimageweixin=(ImageButton) findViewById(R.id.id_tab_weixin_img);
        xmlimagefrd=(ImageButton) findViewById(R.id.id_tab_frd_img);
        xmlimagecontact=(ImageButton) findViewById(R.id.id_tab_contact_img);
        xmlimagesettings=(ImageButton) findViewById(R.id.id_tab_settings_img);
    }
    private void setSelect(int i) {
        FragmentTransaction transaction=fm.beginTransaction();
        hideFragment(transaction);
        switch (i){
            case 0:
                Log.d("SetSelect","1");
                transaction.show(mTab01);
                xmlimageweixin.setImageResource(R.drawable.tab_weixin_pressed);
                break;
            case 1:
                transaction.show(mTab02);
                xmlimagefrd.setImageResource(R.drawable.tab_find_frd_pressed);
                break;
            case 2:
                transaction.show(mTab03);
                xmlimagecontact.setImageResource(R.drawable.tab_address_pressed);
                break;
            case 3:
                transaction.show(mTab04);
                xmlimagesettings.setImageResource(R.drawable.tab_settings_pressed);
                break;
            default:
                break;
        }
        transaction.commit();
    }

    private void hideFragment(FragmentTransaction transaction){
        transaction.hide(mTab01);
        transaction.hide(mTab02);
        transaction.hide(mTab03);
        transaction.hide(mTab04);
    }

    private void initEvent(){
        xmlweixin.setOnClickListener(this);
        xmlfrd.setOnClickListener(this);
        xmlcontact.setOnClickListener(this);
        xmlsettings.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        Log.d("OnClick","1");
        resetImgs();
        switch(view.getId()){
            case R.id.id_tab_weixin:
                Log.d("OnClick","2");
                setSelect(0);
                break;
            case R.id.id_tab_frd:
                setSelect(1);
                break;
            case R.id.id_tab_contact:
                setSelect(2);
                break;
            case R.id.id_tab_settings:
                setSelect(3);
                break;
            default:
                break;
        }
    }

    public void resetImgs(){
        xmlimageweixin.setImageResource(R.drawable.tab_weixin_normal);
        xmlimagefrd.setImageResource(R.drawable.tab_find_frd_normal);
        xmlimagecontact.setImageResource(R.drawable.tab_address_normal);
        xmlimagesettings.setImageResource(R.drawable.tab_settings_normal);
    }
}
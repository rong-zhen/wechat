package com.example.mywechat;

import android.content.Context;
        import android.text.TextUtils;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
        import androidx.recyclerview.widget.RecyclerView;

        import java.util.List;

public class contactAdapter extends RecyclerView.Adapter<contactAdapter.ContactViewHolder> {
    private static final String TAG = contactAdapter.class.getSimpleName();

    private Context mContext;

    private List<contactData> mList;
    public static final int FIRST_STICKY_VIEW = 1;
    public static final int HAS_STICKY_VIEW = 2;
    public static final int NONE_STICKY_VIEW = 3;

    public contactAdapter(Context context) {
        mContext = context;
    }

    public void setContactDataList(List<contactData> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.tab03__recycle_item, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        final String content = mList.get(position).getName();
        contactData stickyData = mList.get(position);

        holder.tvName.setText(stickyData.getName());

        if (position == 0) {
            holder.tvGroup.setVisibility(View.VISIBLE);
            holder.tvGroup.setText(stickyData.group);
            holder.itemView.setTag(FIRST_STICKY_VIEW);
        } else {
            if (!TextUtils.equals(stickyData.group, mList.get(position - 1).group)) {
                holder.tvGroup.setVisibility(View.VISIBLE);
                holder.tvGroup.setText(stickyData.group);
                holder.itemView.setTag(HAS_STICKY_VIEW);
            } else {
                holder.tvGroup.setVisibility(View.GONE);
                holder.itemView.setTag(NONE_STICKY_VIEW);
            }
        }
        holder.itemView.setContentDescription(stickyData.group);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "你点击的是：" + content, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        TextView tvGroup, tvName;

        public ContactViewHolder(View itemView) {
            super(itemView);
            tvGroup = itemView.findViewById(R.id.tab03_header_view);
            tvName = itemView.findViewById(R.id.tab03_name);
        }

    }
}